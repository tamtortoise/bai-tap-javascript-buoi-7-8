var numberList = [];

// Tạo function nhập số vào

function inPut() {
  var number = document.getElementById("txt-number").value * 1;
  numberList.push(number);
  document.getElementById("themSo").innerHTML = numberList;
}

// câu 1: Tổng các số dương trong mảng

function tinhTong() {
  var sum = 0;
  for (var i = 0; i < numberList.length; i++) {
    sum += numberList[i];
  }
  document.getElementById("resultC1").innerHTML = sum;
}

// Câu 2: Đếm số dương trong mảng

function demSoDuong() {
  var SoDuong = 0;
  for (var i = 0; i < numberList.length; i++) {
    var soHienTai = numberList[i];
    if (soHienTai > 0) {
      SoDuong++;
    }
  }

  document.getElementById("resultC2").innerHTML = SoDuong;
}

// Câu 3: Tìm số nhỏ nhất trong mảng

function findSmall() {
  var smolNumb = numberList[0];
  for (var i = 0; i < numberList.length; i++) {
    var soHienTai = numberList[i];
    if (soHienTai < smolNumb) {
      smolNumb = soHienTai;
    }
  }
  document.getElementById("resultC3").innerHTML = smolNumb;
}

// câu 4: Tìm số dương nhỏ nhất

function findSmallPositive() {
  var soDuongBeNhat = numberList[0];
  for (var i = 0; i < numberList.length; i++) {
    soHienTai = numberList[i];
    if (soHienTai > 0 && soHienTai < soDuongBeNhat) {
      soDuongBeNhat = soHienTai;
    }
  }
  document.getElementById("resultC4").innerHTML = soDuongBeNhat;
}

// Câu 5: Tìm số chẵn cuối trong mảng

function findEven() {
  var soChanCuoi = numberList[0];
  for (var i = 0; i < numberList.length; i++) {
    soHienTai = numberList[i];
    if (soHienTai % 2 == 0) {
      soChanCuoi = soHienTai;
    }
    if (soChanCuoi % 2 !== 0) {
      soChanCuoi = -1;
    }
  }
  document.getElementById("resultC5").innerHTML = soChanCuoi;
}

// Câu 7: Sắp xếp theo thứ tự tăng dần

var arr = []; 

function getNumber() {
  var input = document.getElementById("inputNum").value;

  arr.push(input);
  document.getElementById("txtArray").innerHTML = arr;
}

function sortIncrease() {
  arr.sort(function (a, b) {
    return a - b;
  });
  document.getElementById("txtIncrease").innerHTML = arr;
}

// Câu 8 Tìm số nguyên tố đầu tiên

var arr2 = [];

function getNumber2() {
  var input = document.getElementById("inputNum2").value;

  arr2.push(input);
  document.getElementById("txtArray2").innerHTML = arr2;
}

function soNguyen() {
  var ketQua = 0;
  var count = "";
  for (var i = 0; i < arr2.length - 1; i++) {
    if (arr2[i] % 1 == 0) {
      count++;
      break;
    }
  }
  document.getElementById("txt-Prime").innerHTML = arr2[i];
}


